<?php


namespace DefStudio\Stubs;


use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\SplFileInfo;

class StubsPublishCommand extends Command
{
    use ConfirmableTrait;

    protected $signature = 'defstudio-stubs:publish {--force : Overwrite any existing file}';

    protected $description = 'Publish all available stubs';

    public function handle()
    {
        if (!$this->confirmToProceed()) {
            return 1;
        }

        $stubs_path = $this->getLaravel()->basePath('stubs');

        if (!is_dir($stubs_path)) {
            (new Filesystem)->makeDirectory($stubs_path);
        }

        collect(File::files(__DIR__ . '/../stubs'))->each(function (SplFileInfo $file) use ($stubs_path) {
            $source_path = $file->getPathname();

            $target_path = $stubs_path . "/{$file->getFilename()}";

            if (!file_exists($target_path) || $this->option('force')) {
                file_put_contents($target_path, file_get_contents($source_path));
            }
        });

        $this->info('Done!');

        return 0;
    }
}
