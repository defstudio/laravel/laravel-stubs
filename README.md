# Laravel custom stubs collection

## Installation

include the custom repository in composer.json file:

```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/defstudio/laravel/laravel-stubs.git"
    }
]
```

and require via composer:

```bash
composer require defstudio/laravel-stubs --dev
```

then add a composer hook to keep stubs up to date

```json
"scripts": {
    "post-update-cmd": [
        "@php artisan defstudio-stubs:publish"
    ]
}
```

## Usage

publish laravel stubs with the command

```bash
php artisan defstudio-stubs:publish --force
```


## Credits

Tailored on Def Studio needs, starting from [Spatie](https://spatie.be)'s [laravel-stubs](https://github.com/spatie/laravel-stubs)
